#!/usr/bin/python
def start(fileName): 
    file = open(fileName, "r")
    # Part 1
    findoverlapping(file.readlines())


def findoverlapping(entries):
    fabric = [['.']*len(entries) for _ in range(len(entries))]
    for x in range(0, len(entries)):
        splitstring = entries[x].split()
        ID = splitstring[0].split('#')        
        offsetX = int(splitstring[2].split(',')[0])
        offsetY = int(splitstring[2].split(',')[1].split(':')[0])
        length = int(splitstring[3].split('x')[0])
        height = int(splitstring[3].split('x')[1])

        for posX in range(offsetX, offsetX + length):
            for posY in range(offsetY, offsetY + height):
                if fabric[posX][posY] == '.':
                    fabric[posX][posY] = ID
                else :
                    fabric[posX][posY] = 'x'

    #printoverlapping(entries, fabric)

    checkunique(entries, fabric)

def checkunique(entries, fabric):
    for x in range(0, len(entries)):
        splitstring = entries[x].split()
        ID = splitstring[0].split('#')[1]        
        offsetX = int(splitstring[2].split(',')[0])
        offsetY = int(splitstring[2].split(',')[1].split(':')[0])
        length = int(splitstring[3].split('x')[0])
        height = int(splitstring[3].split('x')[1])

        isunique = True
        for posX in range(offsetX, offsetX + length):
            for posY in range(offsetY, offsetY + height):
                if fabric[posX][posY] == 'x':
                    isunique = False 
                    break
        
        if isunique:
            print(ID)
            break


def printoverlapping(entries, fabric):
    counter = 0
   
    for posX in range(0, len(entries)):
            for posY in range(0, len(entries)):
                if fabric[posX][posY] == 'x':
                    counter+=1
    
    print(counter)

start("3-input.txt")