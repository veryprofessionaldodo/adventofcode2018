#!/usr/bin/python
def start(fileName):
    file = open(fileName, "r")
    count(file.readlines())

def count(changes):
    counter = 0
    hasVisited = [0]
    hasFound = False

    while (not hasFound):
        for x in range(0, len(changes)):
            counter += int(changes[x])
        
            if counter in hasVisited:
                hasFound = True
                break  

            hasVisited.append(counter)

    print(counter)

start("1-input.txt")