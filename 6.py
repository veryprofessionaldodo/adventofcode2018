#!/usr/bin/python
gridlines = ""
splitcoordinates = []

def start(fileName): 
    file = open(fileName, "r")
    global gridlines
    gridlines = file.readlines()

    global splitcoordinates
    for i in range(0, len(gridlines)):
        pointX = int(gridlines[i].split(',')[0])
        pointY = int(gridlines[i].split(',')[1])
        tmp = []
        tmp.append(pointX)
        tmp.append(pointY)
        splitcoordinates.append(tmp)

    checklargest()

def checklargest():
    grid = []
    for i in range(0, 360):
        tmp = []
        for j in range(0, 360):
            tmp.append(0)
        grid.append(tmp)    

    for i in range(0, len(gridlines)):
        pointX = int(gridlines[i].split(',')[0])
        pointY = int(gridlines[i].split(',')[1])
        grid[pointX][pointY] = i
    
    newgrid = grid

    for i in range(0, 360):
        for j in range(0,360):
            closestnum = gridcheckclosest(i, j)
            newgrid[i][j] = closestnum
    
    evaluatevalid(newgrid)

def evaluatevalid(grid):
    validchoices = [[str(x) for x in range(0, len(gridlines))]]
    tmpcounter = []
    for x in range(0, len(gridlines)):
        tmpcounter.append(0)

    validchoices.append(tmpcounter)

    for i in range(0, 360):
        for j in range(0, 360):
            if (i == 0 or i == 359 or j == 0 or j == 359):
                if grid[i][j] in validchoices[0]:
                    validchoices[0] = remove_values_from_list(validchoices[0], grid[i][j])
                    validchoices[1][int(grid[i][j])] = 0
                
            if grid[i][j] in validchoices[0]:
                validchoices[1][int(grid[i][j])] = validchoices[1][int(grid[i][j])] + 1

    
    print(max(validchoices[1]) + 1)

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]

def gridcheckclosest(y, x):
    mindistance = 3600000

    minnum = -1
    for i in range(0, len(splitcoordinates)):
        pointX = splitcoordinates[i][0]
        pointY = splitcoordinates[i][1]
        
        # is an actual point
        if pointX == x and pointY == y:
            return 'p'

        diffx = abs(x - pointX)
        diffy = abs(y - pointY)

        # is on the same line or column
        if diffx == 0:
            distance = diffy
        elif diffy == 0:
            distance = diffx
        else:
            distance = abs(x - pointX) + abs(y - pointY)
        
        if distance == mindistance:  
            minnum = '.'
        
        if distance < mindistance:  
            mindistance = distance
            minnum = str(i)

    return minnum

start("6-input.txt")