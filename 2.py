#!/usr/bin/python

def start(fileName): 
    file = open(fileName, "r")
    # Part 1
    #checksum(file.readlines())
    # Part 2
    finddifferent(file.readlines())

def checksum(lines):
    counterfortwo = 0
    counterforthree = 0
    for x in range(0, len(lines)):
        if hasMultiple(lines[x],2):
            counterfortwo += 1
        if hasMultiple(lines[x],3):
            counterforthree += 1

    print(counterforthree * counterfortwo)

def hasMultiple(string, value):
    stringdict = {}
    for c in string:
        if c not in stringdict:
            stringdict[c] = 1
        elif c in stringdict:
            stringdict[c] += 1

    return value in stringdict.values()

def finddifferent(lines):
    for x in range(1, len(lines)):
        if checkdifferent(lines[len(lines)-x], lines):
            break


def checkdifferent(string, lines): 
    for x in range(0, len(lines)):
        similar = ""
        for pos in range(0, len(string)):
            if string[pos] == lines[x][pos]:
                similar += string[pos]
        if len(similar) == len(string) - 1:
            print(similar)
            return True
    
    return False 

start("2-input.txt")