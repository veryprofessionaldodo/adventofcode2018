#!/usr/bin/python
def start(fileName): 
    file = open(fileName, "r")
    
    polymer = file.readlines()[0]
    
    # Part 1
    parsepolymer(polymer)
    
    # Part 2
    checkinstablechar(polymer)

def checkinstablechar(polymer):
    alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

    mincount = 1000000
    for i in range(0, len(alphabet)):
        tmppolymer = polymer.replace(alphabet[i], "")
        tmppolymer = tmppolymer.replace(alphabet[i].upper(), "")

        tmppolymer = parsepolymer(tmppolymer)

        if (len(tmppolymer) < mincount):
            mincount = len(tmppolymer)

    print(mincount)


def parsepolymer(polymer):
    isvalidpolymer = True
  
    polymer = deconstructpolymer(polymer)
    
    #print(polymer)
    #print(len(polymer))

    return polymer

def deconstructpolymer(polymer):
    c = 1
    while c < len(polymer):
        
        previouschar = polymer[max(c-1,0)]
        char = polymer[c]
        
        if arevalid(previouschar, char):
            c += 1
        else: 
            
            polymer = polymer[:c-1] + polymer[c+1:]

            #print("depois")
            #print(polymer)
            c = max(c - 1,1)
    return polymer

def arevalid(previouschar, char):
    modifiedchar = char.lower()
    modifiedpreviouschar = previouschar.lower()

    if (modifiedpreviouschar == modifiedchar):
        if (char.islower() and previouschar.isupper()) or (previouschar.islower() and char.isupper()):  
            return False
        else:    
            return True
    else:
        return True

start("4-input.txt")